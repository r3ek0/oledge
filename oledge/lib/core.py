from olobject import OLObject
from input import InputHandler
from display import Display
from widget import ListWidget
from .fonts import calibri_regular_10 as polo_10

import ConfigParser
import sys
import os
import threading

class Core(OLObject):
	def __init__(self,configfile):
		OLObject.__init__(self)
		self.config = ConfigParser.ConfigParser()
		self.config.read(configfile)

		# init the AppManager
		self.appManager = AppManager(self.config)

		# init the inputhandler
		self.inputHandler = InputHandler(self.config)
		self.inputHandler.addListener("input",self.appManager.handleInput)

		# init the display
		self.display = Display()
		self.appManager.addListener("display",self.updateDisplay)
		self.appManager.addListener("startApp",self.startApp)
		self.appManager.addListener("resumeApp",self.display.clear)

		#self.appManager.show()
		self.appManager.fireEvent("startApp")

	def startApp(self):
		print "StartApp."

		self.display.clear()
		self.appManager.show()
		self.display.display()
		print "StartApp done."

	
	def handleInput(self,event):
		self.appManager.handleInput(event)
	
	def updateDisplay(self,*args):
		print "Core: update display."
		self.display.display_block(*args)

class AppManager(OLObject):
	def __init__(self,config):
		OLObject.__init__(self)
		self.running_apps = []
		#self.loadApps()
		self.appdir = config.get('applications','appdir')
		self.apps = os.listdir(self.appdir)
		self.launcher = AppLauncher(self.apps)
		self.launcher.addListener("display",self.updateDisplay)
		self.launcher.addListener("startApp",self.startApp)
		self.focused_app = self.launcher
		sys.path.append(self.appdir)

	def show(self):
		self.focused_app.show()

	def handleInput(self,event):
		if not self.focused_app:
			return

		name,action = event
		#print "name: %s | action %s | %s" % (name,action,self.focused_app)
		# event should be routed to the focused_app
		self.focused_app.handleInput(event)

	def updateDisplay(self,*data):
			print "AppManager: update display"
			self.fireEvent("display",*data)

	def startApp(self,appname):
		self.fireEvent("startApp")
		mod = __import__(appname)
		app = eval("mod." + appname + "()")
		self.focused_app.removeListener("display",self.updateDisplay)
		app.addListener("display",self.updateDisplay)
		app.addListener("quit",self.stopApp)
		self.running_apps.append(app)
		self.focused_app = app
	
	def stopApp(self,app):
		for ra in self.running_apps:
			if ra is app:
				self.running_apps.remove(ra)
				del ra
				self.resumeApp(self.launcher)

	def resumeApp(self,app):
		self.fireEvent("resumeApp")
		app.addListener("display",self.updateDisplay)
		app.show()
		self.focused_app = app


class Application(OLObject):
	def __init__(self):
		OLObject.__init__(self)
		self.activity_stack = []
		self.current_activity = False

	def handleInput(self,event):
		if not self.current_activity:
			return

		self.current_activity.handleInput(event)
	def show(self):
		self.current_activity.show()

	def updateDisplay(self,*data):
			print "Application: Update display."
			self.fireEvent("display",*data)

	def startActivity(self,activity,*args):
		#if self.current_activity:
		#	del self.current_activity

		act = activity(*args)
		act.addListener("display",self.updateDisplay)
		self.current_activity = act
		self.show()

class Activity(OLObject):
	def __init__(self):
		OLObject.__init__(self)
		self.widgets = []
	
	def updateDisplay(self,*data):
			print "Activity: Update display."
			#print data
			self.fireEvent("display",*data)

	def addWidget(self,widget):
		for w in self.widgets:
			if w is widget:
				return
		widget.addListener("update",self.updateDisplay)
		self.widgets.append(widget)

	def handleInput(self,event):
		pass

	def show(self):
		for w in self.widgets:
			w.updateWidget()

class Service(OLObject):
	def __init__(self):
		OLObject.__init__(self)
		self.thread = 0
		self.running = False

	def start(self):
		self.running = True
		self.thread = threading.Thread(target=self.serve)
		self.thread.daemon = False
		self.thread.start()

	def stop(self):
		self.running = False
	
	def __del__(self):
		self.stop()

	def serve(self):
		# this mathod must be implemented by the user
		pass
	



#----------------------------------------------------------------------

class AppLauncher(Application):
	def __init__(self,apps=["test qwe 123","Foo Bar","Radio","Mp3"]):
		Application.__init__(self)		
		self.apps = []
		self.getAppList(apps)
		self.startActivity(AppListView,self.apps)
		self.current_activity.addListener("selection",self.startApp)
	
	def getAppList(self,apps):
		for app in apps:
			if app == "__init__.py":
				continue
			if app[(len(app)-3):len(app)] != ".py":
				continue

			self.apps.append(app[:-3])

	def startApp(self,app):
		self.fireEvent("startApp",app)

class AppListView(Activity):
	def __init__(self,apps):
		Activity.__init__(self)
		self.applist = ListWidget(0,16,128,40,polo_10,apps)
		self.applist.addListener("selection",self.selection)
		self.addWidget(self.applist)
		
	def selection(self,*args):
		#print args
		self.fireEvent("selection",*args)


	def handleInput(self,event):
		name,action = event
		if action is "buttonUp":
			return
		#print "In AppListView: \"%s\" \"%s\"" % (name,action)
		if name == "button_2":
			#print "scrollDown"
			self.applist.scrollDown()
		
		if name == "button_3":
			#print "scrollUp"
			self.applist.scrollUp()
		
		if name == "button_4":
			self.applist.selectItem()

		if name == "turn" and action == "cw":
			self.applist.scrollUp()

		if name == "turn" and action == "ccw":
			self.applist.scrollDown()
