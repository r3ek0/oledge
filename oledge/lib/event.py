class EventHandler:
	
	def __init__(self):
		self.callbacks = []
		self.active = True
	
	def toggleEventHandler(toggle):
			self.active = toggle

	def addListener(self,cbtype,callback):
		for ( t, c ) in self.callbacks:
			if t == cbtype and callback == c:
				return
	
		cb = (cbtype,callback)
		self.callbacks.append(cb)
	
	def fireEvent(self,cbtype, *args):
		if not self.active:
			return

		for ( t, c ) in self.callbacks:
			if t == cbtype:
				c(*args)
	
	def removeListener(self,cbtype,callback):
		for ( t, c ) in self.callbacks:
			if t == cbtype and callback == c:
				self.callbacks.remove((t,c))
	
