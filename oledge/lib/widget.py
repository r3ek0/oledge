from olobject import OLObject
from bitmap import Bitmap

class OLWidget(OLObject):
	
	def __init__(self,x=0,y=0,w=128,h=64,offset=0):
		OLObject.__init__(self)
		self.x = 0
		self.y = 0
		self.row = 0
		self.column = 0
		self.setPosition(x,y)
		self.offset = offset
		self.width = self.x + w
		self.height = h
		self.bmp = Bitmap(w,h)
	
	def setPosition(self,x,y):

		self.row = (y/8)*8
		self.y = y % 8
		self.x = x
		self.column = x
		print "        x   y   row col"
		print "input : %3d %3d %3d %3d" % (x,y,0,0)
		print "output: %3d %3d %3d %3d" % (self.x,self.y,self.row,self.column)

	def setWidth(self,width):
		self.width = width
	
	def setHeight(self,height):
		self.height = height

	def updateWidget(self):
		# we pass the args for display_block
		# x -> start col
		# y -> start row
		# w -> col_count
		# h -> rows
		# bitmap:     instance of Bitmap
		#             The number of rows in the bitmap must be a multiple of 8.
		# row:        Starting row to write to - must be multiple of 8
		# col:        Starting col to write to.
		# col_count:  Number of cols to write.
		# col_offset: column offset in buffer to write from
		#  
		#def display_block(self, bitmap, row, col, col_count, col_offset=0):

		self.fireEvent(	"update", 
			self.bmp,
			self.row,
			self.column,
			self.width,
			self.offset) 
	
class Text(OLWidget):
	
	def __init__(self,x,y,w,h,text="",font=""):
		OLWidget.__init__(self,x,y,w,h)
		self.text = text
		self.font = font
		self.wtype = "text"
		self.bmp = Bitmap(w,h)

	def setText(self,text):
		self.text = text
		self.bmp.clear()
		w = self.bmp.draw_text(self.x,self.y,self.text,self.font)
		self.updateWidget()


class Button(OLWidget):
	
	def __init__(self,x,y,w,h,label):
		OLWidget.__init__(self,x,y,w,h)
		self.label = label
		self.wtype = "button"
		self.bmp = Bitmap(w,h)
		
	def setLabel(self,label):
		self.label = label
		self.bmp.draw_text(self.x,self.y,self.label,self.font)
		self.updateWidget()

	
class ListWidget(OLWidget):
	def __init__(self,x,y,w,h,font,items=[],size=3):
		OLWidget.__init__(self,x,y,w,h)
		self.items = []
		self.font = font
		self.size = size
		self.selection = False
		self.bitmaps = []
		self.wtype = "list"
		if len(self.items) <= 0:
			self.addItems(items)

	def addItems(self,items):
		self.items = self.items + items
		self._createBuffers()
		self.updateWidget()

	def addItem(self,item):
		lastitem = len(self.items) -1
		if lastitem < 0:
			lastitem = 0
		self.items.append(item)
		self._createBuffers(start=lastitem)
		if not self.selection:
			self.bmp = self.bitmaps[0]
			self.selection = 0
		self.updateWidget()
			
	def _createBuffers(self,start=0):
		bufferlen = len(self.items)
		self.bitmaps = []
		for i in range(start,bufferlen):
			bitmap = Bitmap(self.width,self.height)

			if self.size == 1:
				text = "%s" % self.items[i]
				bitmap.draw_text(self.x,self.y,text,self.font)
				#bitmap.draw_text(self.x + 12,self.y,text,self.font)
				#bitmap.draw_triangle(self.x,self.y + 3,self.x,self.y + 8,self.x + 5,self.y + 5,1)
				self.bitmaps.append(bitmap)
				continue
			
			preel = i - 1
			selel = i
			postel = i +1

			if preel < 0:
				preel = preel + bufferlen
			if postel is bufferlen or postel > bufferlen:
				postel = 0 
			
			if self.items[preel]:
				bitmap.draw_text(12,0,self.items[preel],self.font)
			text = "%s" % self.items[selel]
			bitmap.draw_text(12,14,text,self.font)
			bitmap.draw_triangle(0,17,0,23,5,20,1)	
			if self.items[postel]:
				bitmap.draw_text(12,27,self.items[postel],self.font)
			self.bitmaps.append(bitmap)

	def scrollDown(self):
		self.selection += 1
		if self.selection is len(self.items):
			self.selection = 0
		self.bmp = self.bitmaps[self.selection]
		self.updateWidget()


	def scrollUp(self):
		self.selection -= 1
		if self.selection < 0:
			self.selection = len(self.items) - 1
		self.bmp = self.bitmaps[self.selection]
		self.updateWidget()

	def selectItem(self):
		self.fireEvent("selection",self.items[self.selection])
