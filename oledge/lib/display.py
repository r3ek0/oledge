from .ssd1306 import SSD1306

class Display(SSD1306):
	
	RESET_PIN = 15
	DC_PIN = 16

	def __init__(self):
		SSD1306.__init__(
			self,
			reset_pin=self.RESET_PIN,
			dc_pin=self.DC_PIN)

		self.begin()
		self.clear_display()
		self.display()
		
