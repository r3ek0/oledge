import wiringpi
import time
import threading
from event import EventHandler
from olobject import OLObject

class InputHandler(OLObject):
	def __init__(self,config):
		OLObject.__init__(self)
		if len(config.items('buttons')) > 0:
			self.buttonHandler = ButtonHandler()
			self.buttonHandler.start()
			for k,v in config.items('buttons'):
				button = SwitchButton(name=k,pin=int(v))
				button.addListener('buttonDown',self.buttonPress)
				button.addListener('buttonUp',self.buttonPress)
				self.buttonHandler.addButton(button)

		if len(config.items('encoder')) > 0:
			pin_a = config.get('encoder','pin_a')
			pin_b = config.get('encoder','pin_b')
			encoder = RotaryEncoder(int(pin_a),int(pin_b))
			encoder.start()
			encoder.addListener('turn',self.turn)

	def buttonPress(self,event):
		self.fireEvent("input",event)
                                                                       
	def turn(self,direction):
		self.fireEvent("input",("turn",direction))      

#-------------------------------------------------------------------------------
# ButtonHandler
#-------------------------------------------------------------------------------
class ButtonHandler(OLObject):
	def __init__(self,gpio=False):
		if not gpio:
			wiringpi.wiringPiSetup()
			self.gpio = wiringpi.GPIO(wiringpi.GPIO.WPI_MODE_PINS)
		else:
			self.gpio = gpio  	

		self.listening = 0
		self.thread = 0
		self.buttons = []

	def addButton(self,button):
		if not button:
			return False
		else:
			for b in self.buttons:
				if b == button:
					return False
			self.gpio.pinMode(button.pin,self.gpio.INPUT)
			self.buttons.append(button)

	def removeButton(self,button):
		if not button:
			return False
		else:
			for b in self.buttons:
				if b == button:
					self.buttons.remove(button)
	
	def listen(self):
		while self.listening == True:
			if len(self.buttons) > 0: 
				for button in self.buttons:
					button.state = self.gpio.digitalRead(button.pin)
					#print "button: %s -> %d" % (b.name , state)
					if button.state == 1 and button.previous_state == 0:
						#print "button: %s (%d)" % (button.name,button.state)
						button.fireEvent("buttonUp",(button.name,"buttonUp"))
						button.previous_state = button.state
	
					if button.state == 0 and button.previous_state == 1:
						#print "%s[%d]: %d (%d)" % (b.name,b.pin,b.state,b.previous_state)
						button.fireEvent("buttonDown",(button.name,"buttonDown"))
						button.previous_state = button.state
					
					time.sleep(0.01)
			else:
				time.sleep(0.1)

	def start(self):
		self.listening = True
		self.thread = threading.Thread(target=self.listen)
		self.thread.daemon = False
		self.thread.start()
	
	def stop(self):
		self.listening = False
		
	def __del__(self):
		self.stop()

#-------------------------------------------------------------------------------
# SwitchButton
#-------------------------------------------------------------------------------
class SwitchButton(OLObject):

	def __init__(self,name="",pin=-1):
		
		if not name or pin < 1:
			return False

		OLObject.__init__(self)
		self.name = name
		self.pin = pin
		self.state = 1
		self.previous_state = 1

class RotaryEncoder(OLObject):
	CW_SEQ = 0
	CCW_SEQ = 1

	def __init__(self,pin_a,pin_b,gpio=False):
		OLObject.__init__(self)
		self.pin_a = pin_a #8
		self.pin_b = pin_b #9
		
		if not gpio:
			wiringpi.wiringPiSetup()
			self.gpio = wiringpi.GPIO(wiringpi.GPIO.WPI_MODE_PINS)
		else:
			self.gpio = gpio  	

		self.listening = 0
		self.thread = 0

		self.state_a = 2
		self.state_b = 2

		self.counter = 0
		self.cw_seq = [ 1, 0, 2, 3 ]
		self.ccw_seq = [ 2, 0, 1, 3]
		self.direction = self.CCW_SEQ
		self.direction_seq = [[ 1, 0, 2, 3 ],[ 2, 0, 1, 3]]

		self.gpio.pinMode(self.pin_a, self.gpio.INPUT)
		self.gpio.pullUpDnControl(self.pin_a, self.gpio.PUD_UP)
		
		self.gpio.pinMode(self.pin_b, self.gpio.INPUT)
		self.gpio.pullUpDnControl(self.pin_b, self.gpio.PUD_UP)


	def start(self):
		self.listening = True
		self.thread = threading.Thread(target=self.listen)
		self.thread.daemon = False
		self.thread.start()
	
	def stop(self):
		self.listening = False
		
	def __del__(self):
		self.stop()
	

	def listen(self):
		while self.listening:
			self.updateEncoders()
			time.sleep(0.001)

	def updateEncoders(self):
		current_a = self.gpio.digitalRead(self.pin_a) 
		current_b = self.gpio.digitalRead(self.pin_b)

		if current_a is not self.state_a or current_b is not self.state_b:
			self.state_a = current_a
			self.state_b = current_b
			#print "a: %d b: %d" % (current_a,current_b)
			if self.counter == 0 and current_b == 1 and current_a == 0:
				self.direction = self.CW_SEQ
				self.counter += 1
				#print "Clockwise"
			elif self.counter == 0 and current_b == 0 and current_a == 1:
				self.direction = self.CCW_SEQ
				self.counter += 1
				#print "Counter Clockwise"
			if self.counter == 3:
				self.counter = 0
				if self.direction == self.CW_SEQ:
					direction = "cw"
				else:
					direction = "ccw"
				print "Turn -> %s" % direction 
				self.fireEvent("turn",direction)

			if self.counter > 0:
				expected = self.direction_seq[self.direction][self.counter]
				received = ( current_a << 1 ) | current_b
				if expected == received:
					self.counter +=1
				

			if self.state_a == 1 and self.state_b == 1:
				self.counter = 0
