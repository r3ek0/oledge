#!/usr/bin/env python

from distutils.core import setup

setup(name='oledge',
      version='0.1',
      description='Python framework for building guis on 128x64 pixel devices',
      author='The Reeko',
      author_email='reeko@hcuats.de',
      url='',
      #license = 'LICENSE.txt',
      #long_description=open('README.txt').read(),
      packages=['oledge','oledge.lib','oledge.lib.fonts'],
      #packages=['oledge','oledge.lib','oledge.lib.fonts'],
      #package_dir = {'': 'lib'}

)
