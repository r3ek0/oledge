#!/usr/bin/python2
import gaugette.ssd1306
import time
import sys
import gaugette.fonts.arial_16
import gaugette.fonts.polo_regular_12
import gaugette.fonts.calibri_regular_14
import gaugette.fonts.calibri_regular_10

RESET_PIN = 15
DC_PIN    = 16
offset = 0

led = gaugette.ssd1306.SSD1306(reset_pin=RESET_PIN, dc_pin=DC_PIN)
led.begin()
led.command(0xAF)
