from oledge.lib.display import Display
from oledge.lib.buffer import OLBuffer
from oledge.lib.widget import  Text
from oledge.lib.widget import  Button
import oledge.lib.fonts.arial_regular_10 as arial_10
import oledge.lib.fonts.polo_regular_12 as polo_12
import oledge.lib.fonts.calibri_regular_10 as calibri_10
import sys

display = Display()
buffer  = OLBuffer()
textfield = Text(0,0,"Textfield test")
textfield2 = Text(15, 10, "Field TEST", polo_12)
textfield3 = Text(5, 24, "LOL foobar", calibri_10)
button = Button(0,50,"Label")
buffer.addWidget(button)
buffer.addWidget(textfield)
buffer.addWidget(textfield2)
buffer.addWidget(textfield3)


buffer.drawBuffer()
display.show(buffer.buffer)
buffer.addListener("refresh",display.show)

while True:
	
	text = sys.stdin.readline().strip()
	if text == "break":
		break
	else:
		textfield2.setText(text)
