from oledge.lib.display import Display
from oledge.lib.buffer import OLBuffer
from oledge.lib.widget import  Text
from oledge.lib.widget import  Button
from oledge.lib.widget import  List
from oledge.lib.widget import  ListItem
from oledge.lib.input import ButtonHandler
from oledge.lib.input import SwitchButton
import oledge.lib.fonts.arial_regular_10 as arial_10
import oledge.lib.fonts.polo_regular_10 as polo_10
import oledge.lib.fonts.calibri_regular_10 as calibri_10
import oledge.lib.fonts.lucida_sans_regular_10 as lucida_10
import oledge.lib.fonts.segoe_print_regular_10 as segoe_10
import oledge.lib.fonts.tahoma_regular_10 as tahoma_10
import sys

def buttonBack(): print "button back released"
def buttonUp(): print "button up released"
def buttonDown(): print "button down released"
def buttonEnter(): print "button enter released"

display = Display()
buffer  = OLBuffer()
buffer.addListener("refresh",display.show)


mylist = List(0,0,5)
items = []
for i in range(0,16):
	name = "Polo 12 - %d" % i
	value = i
	font = ""
	if i % 2 == 0:
		font = arial_10
		name = "Arial 10 - %d" % i
	if i % 3 == 0:
		font = lucida_10
		name = "Lucida 10 - %d" % i
	if i % 4 == 0:
		font = calibri_10
		name = "Calibri 10 - %d" % i
	if i % 5 == 0:
		font = tahoma_10
		name = "Tahoma 10 - %d" % i
	if i % 6 == 0:
		font = segoe_10
		name = "Segoe 10 - %d" % i	
	font = calibri_10
	font = ""
	item = ListItem(name,value,font=font)
	mylist.addItem(item)

buffer.addWidget(mylist)

buffer.drawBuffer()
display.show(buffer.buffer) 

buttonhandler = ButtonHandler()
buttonhandler.start()

def testbuttonpress():
	print "button pressed"
	mylist.scrollUp()
upbutton = SwitchButton(name="Up",pin=4)
upbutton.addListener("buttonDown",mylist.scrollUp)
buttonhandler.addButton(upbutton)

downbutton = SwitchButton(name="Down",pin=5)
downbutton.addListener("buttonDown",mylist.scrollDown)
buttonhandler.addButton(downbutton)


while True:
	
	text = sys.stdin.readline().strip()
	if text == "break":
		break
	else:
		mylist.scroll()
		#textfield2.setText(text)

buttonhandler.stop()
