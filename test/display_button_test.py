from oledge.lib.display import Display
from oledge.lib.buffer import OLBuffer
from oledge.lib.widget import  Text
from oledge.lib.widget import  Button
from oledge.lib.input import ButtonHandler
from oledge.lib.input import SwitchButton
import oledge.lib.fonts.arial_regular_10 as arial_10
import oledge.lib.fonts.polo_regular_12 as polo_12
import oledge.lib.fonts.calibri_regular_10 as calibri_10
import sys

def buttonBack(): print "button back released"
def buttonUp(): print "button up released"
def buttonDown(): print "button down released"
def buttonEnter(): print "button enter released"

display = Display()
buffer  = OLBuffer()
textfield = Text(0,0,"Textfield test")
textfield2 = Text(15, 10, "Field TEST", polo_12)
textfield3 = Text(5, 24, "LOL foobar", calibri_10)
#button = Button(0,50,"Label")
#buffer.addWidget(button)
buffer.addWidget(textfield)
buffer.addWidget(textfield2)
buffer.addWidget(textfield3)


buffer.drawBuffer()
display.show(buffer.buffer)
buffer.addListener("refresh",display.show)

buttonhandler = ButtonHandler()
buttonhandler.start()
 
backbutton = SwitchButton(name="Back",pin=1)
backbutton.addListener("buttonUp",textfield.setText)
buttonhandler.addButton(backbutton)

upbutton = SwitchButton(name="Up",pin=4)
upbutton.addListener("buttonUp",textfield.setText)
buttonhandler.addButton(upbutton)

downbutton = SwitchButton(name="Down",pin=5)
downbutton.addListener("buttonUp",textfield.setText)
buttonhandler.addButton(downbutton)

enterbutton = SwitchButton(name="Enter",pin=6)
enterbutton.addListener("buttonUp",textfield.setText)
buttonhandler.addButton(enterbutton)


while True:
	
	text = sys.stdin.readline().strip()
	if text == "break":
		break
	else:
		textfield2.setText(text)

buttonhandler.stop()
