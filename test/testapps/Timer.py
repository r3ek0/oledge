import time
from oledge.lib.core import Application, Activity, Service
from oledge.lib.widget import Text
from oledge.lib.widget import ListWidget
import oledge.lib.fonts.polo_regular_10 as arial


class Timer(Application):
	def __init__(self):
		Application.__init__(self)
		self.minutes = 0
		self.seconds = 0

		self.timerservice = TimerService()
		self.startActivity(TimerSetView)


		self.timersetview = self.current_activity
		self.timersetview.addListener("setupDone",self.startTimer)
		self.timersetview.addListener("quit",self.quit)
		
		self.timerview = TimerView()
		self.fireEvent("ready")

		
	
	def startTimer(self,args):
		mins,secs = args
		#start the timeer view
		self.timerservice.setTimer(mins,secs)
		self.timerservice.addListener("update",self.serviceUpdate)
		self.timerservice.start()
		self.startActivity(TimerView)
		self.timerview = self.current_activity
		#start the timer service with givern parameters

		


	def quit(self):
		self.timerservice.stop()
		self.fireEvent("quit",self)

	def serviceUpdate(self,args):
		print args
		#print self.timerview.text.setText

		self.timerview.text.setText(str(args))

class TimerService(Service):
	def __init__(self):
		Service.__init__(self)
		self.secs = 0
	def setTimer(self,mins,secs):
		self.secs = (int(mins) * 60) + int(secs)


	def serve(self):
		while self.running:
			self.fireEvent("update",self.secs)
			self.secs -= 1
			if self.secs < 0:
				self.fireEvent("timeup")
				self.stop()
			time.sleep(1)

class TimerSetView(Activity):
	def __init__(self):
		Activity.__init__(self)
		minutes = []
		seconds = []
		for mins in range(0,60):
			minutes.append("%d" % mins)

		for secs in range(0,60):
			seconds.append(str(secs))
		self.minutesspinner = ListWidget(0,16,20,16,arial,minutes,size=1)
		self.secondsspinner = ListWidget(16,16,20,16,arial,seconds,size=1)
		
		self.minutesspinner.addListener("selection",self.setMinutes)
		self.secondsspinner.addListener("selection",self.setSeconds)

		self.addWidget(self.minutesspinner)
		self.addWidget(self.secondsspinner)

		self.mins = 0
		self.secs = 0
		self.toSet = [self.minutesspinner,self.secondsspinner]
		self.focus = 0

	def setMinutes(self,mins):
		print "minutes %s" % mins
		self.mins = mins
	def setSeconds(self,secs):
		print "seconds %s" %secs
		self.secs = secs



	def handleInput(self,event):
		button,action = event
		if action == "buttonUp":
			return
		if button == "turn" and action == "cw":
			self.toSet[self.focus].scrollDown()
		if button == "turn" and action == "ccw":
			self.toSet[self.focus].scrollUp()

		if button == "button_4":
			print self.toSet[self.focus]
			self.toSet[self.focus].selectItem()
			if self.focus == 1:
				self.fireEvent("setupDone",(self.mins,self.secs))

			self.focus += 1


			
class TimerView(Activity):
	def __init__(self):
		Activity.__init__(self)
		self.text = Text(0,27,128,16,"Test",arial)
		self.addWidget(self.text)
		self.text.updateWidget()

	def updateView(self,seconds):
		mins = seconds / 60
		secs = seconds % 60
		self.text.setText(mins + ":" + seconds)
			
	def handleInput(self,event):
		name,action = event
		if action == "buttonUp" and name == "button_1":
			self.fireEvent("quit")
