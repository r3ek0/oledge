import time
from oledge.lib.core import Application, Activity, Service
from oledge.lib.widget import Text
import oledge.lib.fonts.polo_regular_10 as arial


class Clock(Application):
	def __init__(self):
		Application.__init__(self)
		self.minutes = 0
		self.seconds = 0

		self.clockservice = ClockService()
		self.startActivity(ClockView)

		self.clockview = self.current_activity
		self.clockview.addListener("quit",self.quit)

		self.clockservice.addListener("update",self.serviceUpdate)
		self.clockservice.start()

	def quit(self):
		self.clockservice.stop()
		self.fireEvent("quit",self)

	def serviceUpdate(self,*args):
		#print args
		#print self.clockview.text.setText
		self.clockview.text.setText(*args)

class ClockService(Service):
	def __init__(self):
		Service.__init__(self)

	def serve(self):
		while self.running:
			timestr = time.strftime("%H:%M:%S")
			self.fireEvent("update",timestr)
			time.sleep(1)
					

class ClockView(Activity):
	def __init__(self):
		Activity.__init__(self)
		self.text = Text(0,27,128,16,"Test",arial)
		self.addWidget(self.text)
		self.text.updateWidget()
	
	def handleInput(self,event):
		name,action = event
		if action == "buttonUp" and name == "button_1":
			self.fireEvent("quit")
