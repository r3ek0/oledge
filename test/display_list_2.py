from oledge.lib.display import Display
from oledge.lib.widget import  Text
from oledge.lib.widget import  List
from oledge.lib.input import ButtonHandler
from oledge.lib.input import SwitchButton
import oledge.lib.fonts.arial_regular_10 as arial_10
import oledge.lib.fonts.polo_regular_10 as polo_10
import oledge.lib.fonts.calibri_regular_10 as calibri_10
import oledge.lib.fonts.lucida_sans_regular_10 as lucida_10
import oledge.lib.fonts.segoe_print_regular_10 as segoe_10
import oledge.lib.fonts.tahoma_regular_10 as tahoma_10
import sys

def buttonBack(): print "button back released"
def buttonUp(): print "button up released"
def buttonDown(): print "button down released"
def buttonEnter(): print "button enter released"

display = Display()

text = Text(0,0,128,16,"Test",arial_10)
text.addListener("update",display.display_block)
text.setText("Test text 123")

items = []
for i in range(0,16):
	name = "Item %d" % i
	items.append(name)

mylist = List(0,16,128,40,arial_10,items)
mylist.addListener("update",display.display_block)
mylist.updateWidget()

#mylist.addItem("test 123 -foo")

buttonhandler = ButtonHandler()
buttonhandler.start()

def testbuttonpress():
	print "button pressed"
	mylist.scrollUp()


upbutton = SwitchButton(name="Up",pin=5)
upbutton.addListener("buttonDown",mylist.scrollUp)
buttonhandler.addButton(upbutton)

downbutton = SwitchButton(name="Down",pin=4)
downbutton.addListener("buttonDown",mylist.scrollDown)
buttonhandler.addButton(downbutton)


while True:
	
	line = sys.stdin.readline().strip()
	if line == "break":
		break
	else:
		#mylist.scroll()
		text.setText(line)

buttonhandler.stop()
